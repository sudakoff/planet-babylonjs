const config = {
  PLANET_RADIUS: 50,
  PLANET_V: 300,//вершины
  MOON_RADIUS: 25,
  SUN_RADIUS: 20
}


if (BABYLON.Engine.isSupported()) {
  const canvas = document.getElementById("canvas")
  const engine = new BABYLON.Engine(canvas, true)
  const scene = new BABYLON.Scene(engine)

  // Камера
  let camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(155, 5, 55), scene)
    camera.inputs.addMouseWheel()
    camera.attachControl(canvas, true)

  // Фон-куб
  let skybox = BABYLON.Mesh.CreateBox("universe", 3000.0, scene)
    let skyboxMaterial = new BABYLON.StandardMaterial("universe", scene)
    //Видимость
    skyboxMaterial.backFaceCulling = false
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("assets/img/cubetexture/universe", scene)
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE
    skyboxMaterial.disableLighting = true
    skybox.material = skyboxMaterial


  //Земля
  let planet = BABYLON.Mesh.CreateSphere("planet", config.PLANET_V, config.PLANET_RADIUS, scene, true)
    planet.position = new BABYLON.Vector3(-250.0, -10,0, -250.0)
    planet.rotation.z = Math.PI;
    planet.applyDisplacementMap("assets/img/earth-height.png", 0, 1)

  //Луна
  let moon = BABYLON.Mesh.CreateSphere("moon", 25, config.MOON_RADIUS, scene)
    moon.parent = planet
    moon.position = new BABYLON.Vector3(-102.0, 0,0, 0.0)

  let moonMat = new BABYLON.StandardMaterial("moonMat", scene)
    moonMat.diffuseTexture = new BABYLON.Texture("assets/img/moon.jpg", scene)
    moon.material = moonMat
    moonMat.bumpTexture = new BABYLON.Texture("assets/img/moon_bump.jpg", scene)
    moonMat.specularTexture = new BABYLON.Texture("assets/img/moon_spec.jpg", scene)

  let lightSourceMesh = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0.0, 0,0, 0.0), scene)
    lightSourceMesh.diffuse = new BABYLON.Color3(0.5, 0.5, 0.5)

  //Точка камеры
  camera.setTarget(BABYLON.Vector3.Zero())

  let planetMat = new BABYLON.ShaderMaterial("planetMat", scene, {
    vertex: "./assets/scripts/elements/planet_vertex",
    fragment: "./assets/scripts/elements/planet_fragment",
  },
  {
      attributes: ["position", "normal", "uv"],
      uniforms: ["world", "worldView", "worldViewProjection", "diffuseTexture", "nightTexture"],
  })

  let diffuseTexture = new BABYLON.Texture("assets/img/earth-diffuse.jpg", scene)
  let nightTexture = new BABYLON.Texture("assets/img/earth-night-o2.png", scene)

    planetMat.setVector3("vLightPosition", lightSourceMesh.position)
    planetMat.setTexture("diffuseTexture", diffuseTexture)
    planetMat.setTexture("nightTexture", nightTexture)
    planetMat.backFaceCulling = false
    planet.material = planetMat

  //Солнце
  let sun = BABYLON.Mesh.CreateSphere("sun", 15, config.SUN_RADIUS, scene, true)
  let sunMaterial = new BABYLON.StandardMaterial("sunMaterial", scene)
  let fireTexture = new BABYLON.FireProceduralTexture("fire", 128, scene)
    fireTexture.fireColors = [
      new BABYLON.Color3(1.0,.7,0.3),
      new BABYLON.Color3(1.0,0.7,0.3),
      new BABYLON.Color3(1.0,.5,0.0),
      new BABYLON.Color3(1.0,.5,0.0),
      new BABYLON.Color3(1.0,1.0,1.0),
      new BABYLON.Color3(1.0,.5,0.0),
    ];

    sunMaterial.emissiveTexture = fireTexture
    sun.material = sunMaterial
    sun.parent = lightSourceMesh

  let godrays = new BABYLON.VolumetricLightScatteringPostProcess('godrays', 1.0, camera, sun, 100, BABYLON.Texture.BILINEAR_SAMPLINGMODE, engine, false)

    godrays.exposure = 0.95
    godrays.decay = 0.96815
    godrays.weight = 0.78767
    godrays.density = 1.0


    scene.collisionsEnabled = true
    camera.checkCollisions = true
    camera.ellipsoid = new BABYLON.Vector3(1, 1, 1)


    sun.checkCollisions = true
    planet.checkCollisions = true
    moon.checkCollisions = true
    skybox.checkCollisions = true

  let postProcess = new BABYLON.FxaaPostProcess("fxaa", 1.0, null, null, engine, true); //fxaa


  engine.runRenderLoop(function() { //перересовываем
    planet.rotation.y+= 0.001
    scene.render() // 60 раз/сек
  })
}
